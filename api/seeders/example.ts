class example {

    handle(finish: Function): void {

        /**
         * This is the ending callback should be called at the end of the function be careful to put it in the right 
         * place in case of async operations
         */
        finish();
    }

}

module.exports = new example();